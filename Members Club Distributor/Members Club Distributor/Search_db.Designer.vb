﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Search_db
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Search_db))
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton15 = New System.Windows.Forms.RadioButton()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.RadioButton17 = New System.Windows.Forms.RadioButton()
        Me.RadioButton5 = New System.Windows.Forms.RadioButton()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.RadioButton18 = New System.Windows.Forms.RadioButton()
        Me.RadioButton7 = New System.Windows.Forms.RadioButton()
        Me.RadioButton8 = New System.Windows.Forms.RadioButton()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.RadioButton16 = New System.Windows.Forms.RadioButton()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.RadioButton9 = New System.Windows.Forms.RadioButton()
        Me.RadioButton10 = New System.Windows.Forms.RadioButton()
        Me.RadioButton19 = New System.Windows.Forms.RadioButton()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.RadioButton20 = New System.Windows.Forms.RadioButton()
        Me.RadioButton11 = New System.Windows.Forms.RadioButton()
        Me.RadioButton12 = New System.Windows.Forms.RadioButton()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.CheckBox8 = New System.Windows.Forms.CheckBox()
        Me.RadioButton41 = New System.Windows.Forms.RadioButton()
        Me.RadioButton42 = New System.Windows.Forms.RadioButton()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.RadioButton38 = New System.Windows.Forms.RadioButton()
        Me.RadioButton39 = New System.Windows.Forms.RadioButton()
        Me.RadioButton40 = New System.Windows.Forms.RadioButton()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.CheckBox7 = New System.Windows.Forms.CheckBox()
        Me.RadioButton36 = New System.Windows.Forms.RadioButton()
        Me.RadioButton37 = New System.Windows.Forms.RadioButton()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.RadioButton34 = New System.Windows.Forms.RadioButton()
        Me.RadioButton35 = New System.Windows.Forms.RadioButton()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.RadioButton32 = New System.Windows.Forms.RadioButton()
        Me.RadioButton33 = New System.Windows.Forms.RadioButton()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.RadioButton30 = New System.Windows.Forms.RadioButton()
        Me.RadioButton31 = New System.Windows.Forms.RadioButton()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.RadioButton28 = New System.Windows.Forms.RadioButton()
        Me.RadioButton29 = New System.Windows.Forms.RadioButton()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.RadioButton22 = New System.Windows.Forms.RadioButton()
        Me.RadioButton27 = New System.Windows.Forms.RadioButton()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.RadioButton23 = New System.Windows.Forms.RadioButton()
        Me.RadioButton24 = New System.Windows.Forms.RadioButton()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.RadioButton13 = New System.Windows.Forms.RadioButton()
        Me.RadioButton14 = New System.Windows.Forms.RadioButton()
        Me.RadioButton21 = New System.Windows.Forms.RadioButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.RadioButton25 = New System.Windows.Forms.RadioButton()
        Me.RadioButton26 = New System.Windows.Forms.RadioButton()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel17.SuspendLayout()
        Me.Panel16.SuspendLayout()
        Me.Panel15.SuspendLayout()
        Me.Panel14.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.SuspendLayout()
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(6, 411)
        Me.TextBox8.Multiline = True
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox8.Size = New System.Drawing.Size(196, 139)
        Me.TextBox8.TabIndex = 32
        Me.TextBox8.Tag = "ToDisable"
        Me.TextBox8.WordWrap = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(3, 395)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(103, 13)
        Me.Label8.TabIndex = 31
        Me.Label8.Text = "Достижениям:"
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(6, 253)
        Me.TextBox7.Multiline = True
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox7.Size = New System.Drawing.Size(196, 139)
        Me.TextBox7.TabIndex = 30
        Me.TextBox7.Tag = "ToDisable"
        Me.TextBox7.WordWrap = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(3, 237)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(71, 13)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Навыкам:"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(6, 214)
        Me.TextBox6.MaxLength = 1000
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(196, 20)
        Me.TextBox6.TabIndex = 28
        Me.TextBox6.Tag = "ToDisable"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 195)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(87, 13)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Должности:"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(6, 172)
        Me.TextBox5.MaxLength = 50
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(196, 20)
        Me.TextBox5.TabIndex = 26
        Me.TextBox5.Tag = "ToDisable"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 156)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(135, 13)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Номеру телефона:"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(6, 133)
        Me.TextBox4.MaxLength = 50
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(196, 20)
        Me.TextBox4.TabIndex = 24
        Me.TextBox4.Tag = "ToDisable"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 117)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 13)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Классу:"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(6, 94)
        Me.TextBox3.MaxLength = 100
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(196, 20)
        Me.TextBox3.TabIndex = 22
        Me.TextBox3.Tag = "ToDisable"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Школе:"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(6, 55)
        Me.TextBox2.MaxLength = 50
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(196, 20)
        Me.TextBox2.TabIndex = 20
        Me.TextBox2.Tag = "ToDisable"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(119, 13)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Дате рождения:"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(6, 16)
        Me.TextBox1.MaxLength = 100
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(196, 20)
        Me.TextBox1.TabIndex = 18
        Me.TextBox1.Tag = "ToDisable"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Имени:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Lucida Console", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label9.Location = New System.Drawing.Point(12, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(98, 16)
        Me.Label9.TabIndex = 33
        Me.Label9.Text = "Поиск по:"
        Me.ToolTip1.SetToolTip(Me.Label9, resources.GetString("Label9.ToolTip"))
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.RadioButton2)
        Me.Panel1.Controls.Add(Me.RadioButton1)
        Me.Panel1.Controls.Add(Me.RadioButton15)
        Me.Panel1.Location = New System.Drawing.Point(208, 15)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(322, 21)
        Me.Panel1.TabIndex = 34
        Me.Panel1.Tag = "ToDisable"
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(114, 4)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(89, 17)
        Me.RadioButton2.TabIndex = 2
        Me.RadioButton2.Text = "Содержит"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(105, 17)
        Me.RadioButton1.TabIndex = 1
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Содержится"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton15
        '
        Me.RadioButton15.AutoSize = True
        Me.RadioButton15.Location = New System.Drawing.Point(209, 2)
        Me.RadioButton15.Name = "RadioButton15"
        Me.RadioButton15.Size = New System.Drawing.Size(113, 17)
        Me.RadioButton15.TabIndex = 3
        Me.RadioButton15.TabStop = True
        Me.RadioButton15.Text = "Точь-в-точь"
        Me.RadioButton15.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.RadioButton17)
        Me.Panel3.Controls.Add(Me.RadioButton5)
        Me.Panel3.Controls.Add(Me.RadioButton6)
        Me.Panel3.Location = New System.Drawing.Point(208, 94)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(322, 21)
        Me.Panel3.TabIndex = 36
        Me.Panel3.Tag = "ToDisable"
        '
        'RadioButton17
        '
        Me.RadioButton17.AutoSize = True
        Me.RadioButton17.Location = New System.Drawing.Point(209, 1)
        Me.RadioButton17.Name = "RadioButton17"
        Me.RadioButton17.Size = New System.Drawing.Size(113, 17)
        Me.RadioButton17.TabIndex = 42
        Me.RadioButton17.Text = "Точь-в-точь"
        Me.RadioButton17.UseVisualStyleBackColor = True
        '
        'RadioButton5
        '
        Me.RadioButton5.AutoSize = True
        Me.RadioButton5.Location = New System.Drawing.Point(114, 3)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(89, 17)
        Me.RadioButton5.TabIndex = 2
        Me.RadioButton5.Text = "Содержит"
        Me.RadioButton5.UseVisualStyleBackColor = True
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = True
        Me.RadioButton6.Checked = True
        Me.RadioButton6.Location = New System.Drawing.Point(3, 3)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(105, 17)
        Me.RadioButton6.TabIndex = 1
        Me.RadioButton6.TabStop = True
        Me.RadioButton6.Text = "Содержится"
        Me.RadioButton6.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.RadioButton18)
        Me.Panel4.Controls.Add(Me.RadioButton7)
        Me.Panel4.Controls.Add(Me.RadioButton8)
        Me.Panel4.Location = New System.Drawing.Point(208, 132)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(322, 21)
        Me.Panel4.TabIndex = 37
        Me.Panel4.Tag = "ToDisable"
        '
        'RadioButton18
        '
        Me.RadioButton18.AutoSize = True
        Me.RadioButton18.Location = New System.Drawing.Point(209, 2)
        Me.RadioButton18.Name = "RadioButton18"
        Me.RadioButton18.Size = New System.Drawing.Size(113, 17)
        Me.RadioButton18.TabIndex = 43
        Me.RadioButton18.Text = "Точь-в-точь"
        Me.RadioButton18.UseVisualStyleBackColor = True
        '
        'RadioButton7
        '
        Me.RadioButton7.AutoSize = True
        Me.RadioButton7.Location = New System.Drawing.Point(114, 4)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(89, 17)
        Me.RadioButton7.TabIndex = 2
        Me.RadioButton7.Text = "Содержит"
        Me.RadioButton7.UseVisualStyleBackColor = True
        '
        'RadioButton8
        '
        Me.RadioButton8.AutoSize = True
        Me.RadioButton8.Checked = True
        Me.RadioButton8.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(105, 17)
        Me.RadioButton8.TabIndex = 1
        Me.RadioButton8.TabStop = True
        Me.RadioButton8.Text = "Содержится"
        Me.RadioButton8.UseVisualStyleBackColor = True
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Checked = True
        Me.RadioButton4.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(105, 17)
        Me.RadioButton4.TabIndex = 1
        Me.RadioButton4.TabStop = True
        Me.RadioButton4.Text = "Содержится"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Location = New System.Drawing.Point(114, 4)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(89, 17)
        Me.RadioButton3.TabIndex = 2
        Me.RadioButton3.Text = "Содержит"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.RadioButton3)
        Me.Panel2.Controls.Add(Me.RadioButton4)
        Me.Panel2.Controls.Add(Me.RadioButton16)
        Me.Panel2.Location = New System.Drawing.Point(208, 54)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(322, 21)
        Me.Panel2.TabIndex = 35
        Me.Panel2.Tag = "ToDisable"
        '
        'RadioButton16
        '
        Me.RadioButton16.AutoSize = True
        Me.RadioButton16.Location = New System.Drawing.Point(209, 4)
        Me.RadioButton16.Name = "RadioButton16"
        Me.RadioButton16.Size = New System.Drawing.Size(113, 17)
        Me.RadioButton16.TabIndex = 4
        Me.RadioButton16.TabStop = True
        Me.RadioButton16.Text = "Точь-в-точь"
        Me.RadioButton16.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Lucida Console", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label10.Location = New System.Drawing.Point(214, 9)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(128, 16)
        Me.Label10.TabIndex = 38
        Me.Label10.Text = "Режим учёта:"
        Me.ToolTip1.SetToolTip(Me.Label10, resources.GetString("Label10.ToolTip"))
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.RadioButton9)
        Me.Panel5.Controls.Add(Me.RadioButton10)
        Me.Panel5.Controls.Add(Me.RadioButton19)
        Me.Panel5.Location = New System.Drawing.Point(208, 172)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(322, 21)
        Me.Panel5.TabIndex = 39
        Me.Panel5.Tag = "ToDisable"
        '
        'RadioButton9
        '
        Me.RadioButton9.AutoSize = True
        Me.RadioButton9.Location = New System.Drawing.Point(114, 4)
        Me.RadioButton9.Name = "RadioButton9"
        Me.RadioButton9.Size = New System.Drawing.Size(89, 17)
        Me.RadioButton9.TabIndex = 2
        Me.RadioButton9.Text = "Содержит"
        Me.RadioButton9.UseVisualStyleBackColor = True
        '
        'RadioButton10
        '
        Me.RadioButton10.AutoSize = True
        Me.RadioButton10.Checked = True
        Me.RadioButton10.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton10.Name = "RadioButton10"
        Me.RadioButton10.Size = New System.Drawing.Size(105, 17)
        Me.RadioButton10.TabIndex = 1
        Me.RadioButton10.TabStop = True
        Me.RadioButton10.Text = "Содержится"
        Me.RadioButton10.UseVisualStyleBackColor = True
        '
        'RadioButton19
        '
        Me.RadioButton19.AutoSize = True
        Me.RadioButton19.Location = New System.Drawing.Point(209, 1)
        Me.RadioButton19.Name = "RadioButton19"
        Me.RadioButton19.Size = New System.Drawing.Size(113, 17)
        Me.RadioButton19.TabIndex = 44
        Me.RadioButton19.TabStop = True
        Me.RadioButton19.Text = "Точь-в-точь"
        Me.RadioButton19.UseVisualStyleBackColor = True
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.RadioButton20)
        Me.Panel6.Controls.Add(Me.RadioButton11)
        Me.Panel6.Controls.Add(Me.RadioButton12)
        Me.Panel6.Location = New System.Drawing.Point(208, 213)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(322, 21)
        Me.Panel6.TabIndex = 40
        Me.Panel6.Tag = "ToDisable"
        '
        'RadioButton20
        '
        Me.RadioButton20.AutoSize = True
        Me.RadioButton20.Location = New System.Drawing.Point(209, 2)
        Me.RadioButton20.Name = "RadioButton20"
        Me.RadioButton20.Size = New System.Drawing.Size(113, 17)
        Me.RadioButton20.TabIndex = 45
        Me.RadioButton20.Text = "Точь-в-точь"
        Me.RadioButton20.UseVisualStyleBackColor = True
        '
        'RadioButton11
        '
        Me.RadioButton11.AutoSize = True
        Me.RadioButton11.Location = New System.Drawing.Point(114, 4)
        Me.RadioButton11.Name = "RadioButton11"
        Me.RadioButton11.Size = New System.Drawing.Size(89, 17)
        Me.RadioButton11.TabIndex = 2
        Me.RadioButton11.Text = "Содержит"
        Me.RadioButton11.UseVisualStyleBackColor = True
        '
        'RadioButton12
        '
        Me.RadioButton12.AutoSize = True
        Me.RadioButton12.Checked = True
        Me.RadioButton12.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton12.Name = "RadioButton12"
        Me.RadioButton12.Size = New System.Drawing.Size(105, 17)
        Me.RadioButton12.TabIndex = 1
        Me.RadioButton12.TabStop = True
        Me.RadioButton12.Text = "Содержится"
        Me.RadioButton12.UseVisualStyleBackColor = True
        '
        'Panel7
        '
        Me.Panel7.AutoScroll = True
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.Panel17)
        Me.Panel7.Controls.Add(Me.Panel16)
        Me.Panel7.Controls.Add(Me.Panel15)
        Me.Panel7.Controls.Add(Me.Panel14)
        Me.Panel7.Controls.Add(Me.Panel13)
        Me.Panel7.Controls.Add(Me.Panel12)
        Me.Panel7.Controls.Add(Me.Panel11)
        Me.Panel7.Controls.Add(Me.Panel10)
        Me.Panel7.Controls.Add(Me.Panel9)
        Me.Panel7.Controls.Add(Me.Panel8)
        Me.Panel7.Controls.Add(Me.Label8)
        Me.Panel7.Controls.Add(Me.TextBox8)
        Me.Panel7.Controls.Add(Me.Label1)
        Me.Panel7.Controls.Add(Me.TextBox1)
        Me.Panel7.Controls.Add(Me.Label2)
        Me.Panel7.Controls.Add(Me.Panel6)
        Me.Panel7.Controls.Add(Me.TextBox2)
        Me.Panel7.Controls.Add(Me.TextBox7)
        Me.Panel7.Controls.Add(Me.Panel5)
        Me.Panel7.Controls.Add(Me.Label3)
        Me.Panel7.Controls.Add(Me.TextBox3)
        Me.Panel7.Controls.Add(Me.Panel4)
        Me.Panel7.Controls.Add(Me.Label4)
        Me.Panel7.Controls.Add(Me.Panel3)
        Me.Panel7.Controls.Add(Me.TextBox4)
        Me.Panel7.Controls.Add(Me.Panel2)
        Me.Panel7.Controls.Add(Me.Label5)
        Me.Panel7.Controls.Add(Me.Panel1)
        Me.Panel7.Controls.Add(Me.TextBox5)
        Me.Panel7.Controls.Add(Me.Label6)
        Me.Panel7.Controls.Add(Me.TextBox6)
        Me.Panel7.Controls.Add(Me.Label7)
        Me.Panel7.Location = New System.Drawing.Point(12, 28)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(799, 372)
        Me.Panel7.TabIndex = 41
        Me.Panel7.Tag = "ToDisable"
        '
        'Panel17
        '
        Me.Panel17.Controls.Add(Me.CheckBox8)
        Me.Panel17.Controls.Add(Me.RadioButton41)
        Me.Panel17.Controls.Add(Me.RadioButton42)
        Me.Panel17.Location = New System.Drawing.Point(536, 411)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(228, 21)
        Me.Panel17.TabIndex = 50
        Me.Panel17.Tag = "ToDisable"
        '
        'CheckBox8
        '
        Me.CheckBox8.AutoSize = True
        Me.CheckBox8.Location = New System.Drawing.Point(98, 2)
        Me.CheckBox8.Name = "CheckBox8"
        Me.CheckBox8.Size = New System.Drawing.Size(130, 17)
        Me.CheckBox8.TabIndex = 4
        Me.CheckBox8.Text = "Учёт регистра"
        Me.CheckBox8.UseVisualStyleBackColor = True
        '
        'RadioButton41
        '
        Me.RadioButton41.AutoSize = True
        Me.RadioButton41.Location = New System.Drawing.Point(42, 4)
        Me.RadioButton41.Name = "RadioButton41"
        Me.RadioButton41.Size = New System.Drawing.Size(49, 17)
        Me.RadioButton41.TabIndex = 2
        Me.RadioButton41.Text = "ИЛИ"
        Me.RadioButton41.UseVisualStyleBackColor = True
        '
        'RadioButton42
        '
        Me.RadioButton42.AutoSize = True
        Me.RadioButton42.Checked = True
        Me.RadioButton42.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton42.Name = "RadioButton42"
        Me.RadioButton42.Size = New System.Drawing.Size(33, 17)
        Me.RadioButton42.TabIndex = 1
        Me.RadioButton42.TabStop = True
        Me.RadioButton42.Text = "И"
        Me.RadioButton42.UseVisualStyleBackColor = True
        '
        'Panel16
        '
        Me.Panel16.Controls.Add(Me.RadioButton38)
        Me.Panel16.Controls.Add(Me.RadioButton39)
        Me.Panel16.Controls.Add(Me.RadioButton40)
        Me.Panel16.Location = New System.Drawing.Point(208, 411)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(322, 21)
        Me.Panel16.TabIndex = 49
        Me.Panel16.Tag = "ToDisable"
        '
        'RadioButton38
        '
        Me.RadioButton38.AutoSize = True
        Me.RadioButton38.Location = New System.Drawing.Point(209, 1)
        Me.RadioButton38.Name = "RadioButton38"
        Me.RadioButton38.Size = New System.Drawing.Size(113, 17)
        Me.RadioButton38.TabIndex = 45
        Me.RadioButton38.Text = "Точь-в-точь"
        Me.RadioButton38.UseVisualStyleBackColor = True
        '
        'RadioButton39
        '
        Me.RadioButton39.AutoSize = True
        Me.RadioButton39.Location = New System.Drawing.Point(114, 4)
        Me.RadioButton39.Name = "RadioButton39"
        Me.RadioButton39.Size = New System.Drawing.Size(89, 17)
        Me.RadioButton39.TabIndex = 2
        Me.RadioButton39.Text = "Содержит"
        Me.RadioButton39.UseVisualStyleBackColor = True
        '
        'RadioButton40
        '
        Me.RadioButton40.AutoSize = True
        Me.RadioButton40.Checked = True
        Me.RadioButton40.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton40.Name = "RadioButton40"
        Me.RadioButton40.Size = New System.Drawing.Size(105, 17)
        Me.RadioButton40.TabIndex = 1
        Me.RadioButton40.TabStop = True
        Me.RadioButton40.Text = "Содержится"
        Me.RadioButton40.UseVisualStyleBackColor = True
        '
        'Panel15
        '
        Me.Panel15.Controls.Add(Me.CheckBox7)
        Me.Panel15.Controls.Add(Me.RadioButton36)
        Me.Panel15.Controls.Add(Me.RadioButton37)
        Me.Panel15.Location = New System.Drawing.Point(536, 253)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(232, 21)
        Me.Panel15.TabIndex = 47
        Me.Panel15.Tag = "ToDisable"
        '
        'CheckBox7
        '
        Me.CheckBox7.AutoSize = True
        Me.CheckBox7.Location = New System.Drawing.Point(98, 2)
        Me.CheckBox7.Name = "CheckBox7"
        Me.CheckBox7.Size = New System.Drawing.Size(130, 17)
        Me.CheckBox7.TabIndex = 55
        Me.CheckBox7.Text = "Учёт регистра"
        Me.CheckBox7.UseVisualStyleBackColor = True
        '
        'RadioButton36
        '
        Me.RadioButton36.AutoSize = True
        Me.RadioButton36.Location = New System.Drawing.Point(42, 4)
        Me.RadioButton36.Name = "RadioButton36"
        Me.RadioButton36.Size = New System.Drawing.Size(49, 17)
        Me.RadioButton36.TabIndex = 2
        Me.RadioButton36.Text = "ИЛИ"
        Me.RadioButton36.UseVisualStyleBackColor = True
        '
        'RadioButton37
        '
        Me.RadioButton37.AutoSize = True
        Me.RadioButton37.Checked = True
        Me.RadioButton37.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton37.Name = "RadioButton37"
        Me.RadioButton37.Size = New System.Drawing.Size(33, 17)
        Me.RadioButton37.TabIndex = 1
        Me.RadioButton37.TabStop = True
        Me.RadioButton37.Text = "И"
        Me.RadioButton37.UseVisualStyleBackColor = True
        '
        'Panel14
        '
        Me.Panel14.Controls.Add(Me.CheckBox6)
        Me.Panel14.Controls.Add(Me.RadioButton34)
        Me.Panel14.Controls.Add(Me.RadioButton35)
        Me.Panel14.Location = New System.Drawing.Point(536, 213)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(232, 21)
        Me.Panel14.TabIndex = 47
        Me.Panel14.Tag = "ToDisable"
        '
        'CheckBox6
        '
        Me.CheckBox6.AutoSize = True
        Me.CheckBox6.Location = New System.Drawing.Point(98, 3)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(130, 17)
        Me.CheckBox6.TabIndex = 54
        Me.CheckBox6.Text = "Учёт регистра"
        Me.CheckBox6.UseVisualStyleBackColor = True
        '
        'RadioButton34
        '
        Me.RadioButton34.AutoSize = True
        Me.RadioButton34.Location = New System.Drawing.Point(42, 4)
        Me.RadioButton34.Name = "RadioButton34"
        Me.RadioButton34.Size = New System.Drawing.Size(49, 17)
        Me.RadioButton34.TabIndex = 2
        Me.RadioButton34.Text = "ИЛИ"
        Me.RadioButton34.UseVisualStyleBackColor = True
        '
        'RadioButton35
        '
        Me.RadioButton35.AutoSize = True
        Me.RadioButton35.Checked = True
        Me.RadioButton35.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton35.Name = "RadioButton35"
        Me.RadioButton35.Size = New System.Drawing.Size(33, 17)
        Me.RadioButton35.TabIndex = 1
        Me.RadioButton35.TabStop = True
        Me.RadioButton35.Text = "И"
        Me.RadioButton35.UseVisualStyleBackColor = True
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.CheckBox5)
        Me.Panel13.Controls.Add(Me.RadioButton32)
        Me.Panel13.Controls.Add(Me.RadioButton33)
        Me.Panel13.Location = New System.Drawing.Point(536, 172)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(232, 21)
        Me.Panel13.TabIndex = 47
        Me.Panel13.Tag = "ToDisable"
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.Location = New System.Drawing.Point(98, 2)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(130, 17)
        Me.CheckBox5.TabIndex = 53
        Me.CheckBox5.Text = "Учёт регистра"
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'RadioButton32
        '
        Me.RadioButton32.AutoSize = True
        Me.RadioButton32.Location = New System.Drawing.Point(42, 4)
        Me.RadioButton32.Name = "RadioButton32"
        Me.RadioButton32.Size = New System.Drawing.Size(49, 17)
        Me.RadioButton32.TabIndex = 2
        Me.RadioButton32.Text = "ИЛИ"
        Me.RadioButton32.UseVisualStyleBackColor = True
        '
        'RadioButton33
        '
        Me.RadioButton33.AutoSize = True
        Me.RadioButton33.Checked = True
        Me.RadioButton33.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton33.Name = "RadioButton33"
        Me.RadioButton33.Size = New System.Drawing.Size(33, 17)
        Me.RadioButton33.TabIndex = 1
        Me.RadioButton33.TabStop = True
        Me.RadioButton33.Text = "И"
        Me.RadioButton33.UseVisualStyleBackColor = True
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.CheckBox4)
        Me.Panel12.Controls.Add(Me.RadioButton30)
        Me.Panel12.Controls.Add(Me.RadioButton31)
        Me.Panel12.Location = New System.Drawing.Point(536, 132)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(232, 21)
        Me.Panel12.TabIndex = 47
        Me.Panel12.Tag = "ToDisable"
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.Location = New System.Drawing.Point(98, 3)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(130, 17)
        Me.CheckBox4.TabIndex = 52
        Me.CheckBox4.Text = "Учёт регистра"
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'RadioButton30
        '
        Me.RadioButton30.AutoSize = True
        Me.RadioButton30.Location = New System.Drawing.Point(42, 4)
        Me.RadioButton30.Name = "RadioButton30"
        Me.RadioButton30.Size = New System.Drawing.Size(49, 17)
        Me.RadioButton30.TabIndex = 2
        Me.RadioButton30.Text = "ИЛИ"
        Me.RadioButton30.UseVisualStyleBackColor = True
        '
        'RadioButton31
        '
        Me.RadioButton31.AutoSize = True
        Me.RadioButton31.Checked = True
        Me.RadioButton31.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton31.Name = "RadioButton31"
        Me.RadioButton31.Size = New System.Drawing.Size(33, 17)
        Me.RadioButton31.TabIndex = 1
        Me.RadioButton31.TabStop = True
        Me.RadioButton31.Text = "И"
        Me.RadioButton31.UseVisualStyleBackColor = True
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.CheckBox3)
        Me.Panel11.Controls.Add(Me.RadioButton28)
        Me.Panel11.Controls.Add(Me.RadioButton29)
        Me.Panel11.Location = New System.Drawing.Point(536, 94)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(232, 21)
        Me.Panel11.TabIndex = 48
        Me.Panel11.Tag = "ToDisable"
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Location = New System.Drawing.Point(98, 3)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(130, 17)
        Me.CheckBox3.TabIndex = 52
        Me.CheckBox3.Text = "Учёт регистра"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'RadioButton28
        '
        Me.RadioButton28.AutoSize = True
        Me.RadioButton28.Location = New System.Drawing.Point(42, 4)
        Me.RadioButton28.Name = "RadioButton28"
        Me.RadioButton28.Size = New System.Drawing.Size(49, 17)
        Me.RadioButton28.TabIndex = 2
        Me.RadioButton28.Text = "ИЛИ"
        Me.RadioButton28.UseVisualStyleBackColor = True
        '
        'RadioButton29
        '
        Me.RadioButton29.AutoSize = True
        Me.RadioButton29.Checked = True
        Me.RadioButton29.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton29.Name = "RadioButton29"
        Me.RadioButton29.Size = New System.Drawing.Size(33, 17)
        Me.RadioButton29.TabIndex = 1
        Me.RadioButton29.TabStop = True
        Me.RadioButton29.Text = "И"
        Me.RadioButton29.UseVisualStyleBackColor = True
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.CheckBox2)
        Me.Panel10.Controls.Add(Me.RadioButton22)
        Me.Panel10.Controls.Add(Me.RadioButton27)
        Me.Panel10.Location = New System.Drawing.Point(536, 54)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(232, 21)
        Me.Panel10.TabIndex = 47
        Me.Panel10.Tag = "ToDisable"
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(98, 3)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(130, 17)
        Me.CheckBox2.TabIndex = 51
        Me.CheckBox2.Text = "Учёт регистра"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'RadioButton22
        '
        Me.RadioButton22.AutoSize = True
        Me.RadioButton22.Location = New System.Drawing.Point(42, 4)
        Me.RadioButton22.Name = "RadioButton22"
        Me.RadioButton22.Size = New System.Drawing.Size(49, 17)
        Me.RadioButton22.TabIndex = 2
        Me.RadioButton22.Text = "ИЛИ"
        Me.RadioButton22.UseVisualStyleBackColor = True
        '
        'RadioButton27
        '
        Me.RadioButton27.AutoSize = True
        Me.RadioButton27.Checked = True
        Me.RadioButton27.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton27.Name = "RadioButton27"
        Me.RadioButton27.Size = New System.Drawing.Size(33, 17)
        Me.RadioButton27.TabIndex = 1
        Me.RadioButton27.TabStop = True
        Me.RadioButton27.Text = "И"
        Me.RadioButton27.UseVisualStyleBackColor = True
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.CheckBox1)
        Me.Panel9.Controls.Add(Me.RadioButton23)
        Me.Panel9.Controls.Add(Me.RadioButton24)
        Me.Panel9.Location = New System.Drawing.Point(536, 15)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(232, 21)
        Me.Panel9.TabIndex = 46
        Me.Panel9.Tag = "ToDisable"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(98, 2)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(130, 17)
        Me.CheckBox1.TabIndex = 3
        Me.CheckBox1.Text = "Учёт регистра"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'RadioButton23
        '
        Me.RadioButton23.AutoSize = True
        Me.RadioButton23.Location = New System.Drawing.Point(42, 4)
        Me.RadioButton23.Name = "RadioButton23"
        Me.RadioButton23.Size = New System.Drawing.Size(49, 17)
        Me.RadioButton23.TabIndex = 2
        Me.RadioButton23.Text = "ИЛИ"
        Me.RadioButton23.UseVisualStyleBackColor = True
        '
        'RadioButton24
        '
        Me.RadioButton24.AutoSize = True
        Me.RadioButton24.Checked = True
        Me.RadioButton24.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton24.Name = "RadioButton24"
        Me.RadioButton24.Size = New System.Drawing.Size(33, 17)
        Me.RadioButton24.TabIndex = 1
        Me.RadioButton24.TabStop = True
        Me.RadioButton24.Text = "И"
        Me.RadioButton24.UseVisualStyleBackColor = True
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.RadioButton13)
        Me.Panel8.Controls.Add(Me.RadioButton14)
        Me.Panel8.Controls.Add(Me.RadioButton21)
        Me.Panel8.Location = New System.Drawing.Point(208, 253)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(322, 21)
        Me.Panel8.TabIndex = 45
        Me.Panel8.Tag = "ToDisable"
        '
        'RadioButton13
        '
        Me.RadioButton13.AutoSize = True
        Me.RadioButton13.Location = New System.Drawing.Point(209, 1)
        Me.RadioButton13.Name = "RadioButton13"
        Me.RadioButton13.Size = New System.Drawing.Size(113, 17)
        Me.RadioButton13.TabIndex = 45
        Me.RadioButton13.Text = "Точь-в-точь"
        Me.RadioButton13.UseVisualStyleBackColor = True
        '
        'RadioButton14
        '
        Me.RadioButton14.AutoSize = True
        Me.RadioButton14.Location = New System.Drawing.Point(114, 4)
        Me.RadioButton14.Name = "RadioButton14"
        Me.RadioButton14.Size = New System.Drawing.Size(89, 17)
        Me.RadioButton14.TabIndex = 2
        Me.RadioButton14.Text = "Содержит"
        Me.RadioButton14.UseVisualStyleBackColor = True
        '
        'RadioButton21
        '
        Me.RadioButton21.AutoSize = True
        Me.RadioButton21.Checked = True
        Me.RadioButton21.Location = New System.Drawing.Point(3, 4)
        Me.RadioButton21.Name = "RadioButton21"
        Me.RadioButton21.Size = New System.Drawing.Size(105, 17)
        Me.RadioButton21.TabIndex = 1
        Me.RadioButton21.TabStop = True
        Me.RadioButton21.Text = "Содержится"
        Me.RadioButton21.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(592, 437)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(119, 23)
        Me.Button1.TabIndex = 42
        Me.Button1.Tag = "ToDisable"
        Me.Button1.Text = "Поиск"
        Me.ToolTip1.SetToolTip(Me.Button1, "Приступить к поиску.")
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(717, 437)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(94, 23)
        Me.Button2.TabIndex = 43
        Me.Button2.Text = "Закрыть"
        Me.ToolTip1.SetToolTip(Me.Button2, "Закрыть окно поиска.")
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(12, 437)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(106, 23)
        Me.Button3.TabIndex = 44
        Me.Button3.Tag = "ToDisable"
        Me.Button3.Text = "Сброс"
        Me.ToolTip1.SetToolTip(Me.Button3, "Сбросить настройки поиска в исходную." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Результаты поиска сброшены не будут.")
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Lucida Console", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label11.Location = New System.Drawing.Point(12, 407)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(158, 16)
        Me.Label11.TabIndex = 45
        Me.Label11.Text = "Условие поиска:"
        Me.ToolTip1.SetToolTip(Me.Label11, "Здесь задаётся условие, при котором элемент базы данных попадёт в результаты поис" &
        "ка.")
        '
        'RadioButton25
        '
        Me.RadioButton25.AutoSize = True
        Me.RadioButton25.Checked = True
        Me.RadioButton25.Location = New System.Drawing.Point(176, 407)
        Me.RadioButton25.Name = "RadioButton25"
        Me.RadioButton25.Size = New System.Drawing.Size(33, 17)
        Me.RadioButton25.TabIndex = 46
        Me.RadioButton25.TabStop = True
        Me.RadioButton25.Tag = "ToDisable"
        Me.RadioButton25.Text = "И"
        Me.ToolTip1.SetToolTip(Me.RadioButton25, "Значения всех учитываемых полей совпадают с соответствующими значениями элемента " &
        "базы данных.")
        Me.RadioButton25.UseVisualStyleBackColor = True
        '
        'RadioButton26
        '
        Me.RadioButton26.AutoSize = True
        Me.RadioButton26.Location = New System.Drawing.Point(215, 407)
        Me.RadioButton26.Name = "RadioButton26"
        Me.RadioButton26.Size = New System.Drawing.Size(49, 17)
        Me.RadioButton26.TabIndex = 47
        Me.RadioButton26.Tag = "ToDisable"
        Me.RadioButton26.Text = "ИЛИ"
        Me.ToolTip1.SetToolTip(Me.RadioButton26, "Значения любого из учитываемых полей совпадают с соответствующими значениями элем" &
        "ента базы данных.")
        Me.RadioButton26.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Lucida Console", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label12.Location = New System.Drawing.Point(549, 9)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(178, 16)
        Me.Label12.TabIndex = 48
        Me.Label12.Text = "Условия сходства:"
        Me.ToolTip1.SetToolTip(Me.Label12, resources.GetString("Label12.ToolTip"))
        '
        'Search_db
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(823, 471)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.RadioButton26)
        Me.Controls.Add(Me.RadioButton25)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Search_db"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Поиск элементов"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel17.ResumeLayout(False)
        Me.Panel17.PerformLayout()
        Me.Panel16.ResumeLayout(False)
        Me.Panel16.PerformLayout()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox8 As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents Panel3 As Panel
    Friend WithEvents RadioButton5 As RadioButton
    Friend WithEvents RadioButton6 As RadioButton
    Friend WithEvents Panel4 As Panel
    Friend WithEvents RadioButton7 As RadioButton
    Friend WithEvents RadioButton8 As RadioButton
    Friend WithEvents RadioButton4 As RadioButton
    Friend WithEvents RadioButton3 As RadioButton
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label10 As Label
    Friend WithEvents Panel5 As Panel
    Friend WithEvents RadioButton9 As RadioButton
    Friend WithEvents RadioButton10 As RadioButton
    Friend WithEvents Panel6 As Panel
    Friend WithEvents RadioButton11 As RadioButton
    Friend WithEvents RadioButton12 As RadioButton
    Friend WithEvents Panel7 As Panel
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents RadioButton17 As RadioButton
    Friend WithEvents RadioButton18 As RadioButton
    Friend WithEvents RadioButton20 As RadioButton
    Friend WithEvents RadioButton19 As RadioButton
    Friend WithEvents RadioButton16 As RadioButton
    Friend WithEvents RadioButton15 As RadioButton
    Friend WithEvents Panel9 As Panel
    Friend WithEvents RadioButton23 As RadioButton
    Friend WithEvents RadioButton24 As RadioButton
    Friend WithEvents Panel8 As Panel
    Friend WithEvents RadioButton13 As RadioButton
    Friend WithEvents RadioButton14 As RadioButton
    Friend WithEvents RadioButton21 As RadioButton
    Friend WithEvents Label11 As Label
    Friend WithEvents RadioButton25 As RadioButton
    Friend WithEvents RadioButton26 As RadioButton
    Friend WithEvents Panel15 As Panel
    Friend WithEvents RadioButton36 As RadioButton
    Friend WithEvents RadioButton37 As RadioButton
    Friend WithEvents Panel14 As Panel
    Friend WithEvents RadioButton34 As RadioButton
    Friend WithEvents RadioButton35 As RadioButton
    Friend WithEvents Panel13 As Panel
    Friend WithEvents RadioButton32 As RadioButton
    Friend WithEvents RadioButton33 As RadioButton
    Friend WithEvents Panel12 As Panel
    Friend WithEvents RadioButton30 As RadioButton
    Friend WithEvents RadioButton31 As RadioButton
    Friend WithEvents Panel11 As Panel
    Friend WithEvents RadioButton28 As RadioButton
    Friend WithEvents RadioButton29 As RadioButton
    Friend WithEvents Panel10 As Panel
    Friend WithEvents RadioButton22 As RadioButton
    Friend WithEvents RadioButton27 As RadioButton
    Friend WithEvents Label12 As Label
    Friend WithEvents Panel17 As Panel
    Friend WithEvents RadioButton41 As RadioButton
    Friend WithEvents RadioButton42 As RadioButton
    Friend WithEvents Panel16 As Panel
    Friend WithEvents RadioButton38 As RadioButton
    Friend WithEvents RadioButton39 As RadioButton
    Friend WithEvents RadioButton40 As RadioButton
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents CheckBox1 As CheckBox
    Friend WithEvents CheckBox8 As CheckBox
    Friend WithEvents CheckBox7 As CheckBox
    Friend WithEvents CheckBox6 As CheckBox
    Friend WithEvents CheckBox5 As CheckBox
    Friend WithEvents CheckBox4 As CheckBox
    Friend WithEvents CheckBox3 As CheckBox
    Friend WithEvents CheckBox2 As CheckBox
End Class
