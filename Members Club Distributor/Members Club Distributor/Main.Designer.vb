﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Help = New System.Windows.Forms.ToolTip(Me.components)
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Button31 = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Button33 = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ListBox3 = New System.Windows.Forms.ListBox()
        Me.Sorting_timer = New System.Windows.Forms.Timer(Me.components)
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.ListBox7 = New System.Windows.Forms.ListBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.ListBox5 = New System.Windows.Forms.ListBox()
        Me.ListBox6 = New System.Windows.Forms.ListBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Button27 = New System.Windows.Forms.Button()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Button30 = New System.Windows.Forms.Button()
        Me.Button29 = New System.Windows.Forms.Button()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Key = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImageDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewImageColumn()
        Me.NameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SchoolDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.YearsDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TelephoneDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PostDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContextMenu_db1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ПросмотрToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ДобавитьКРаспределениюToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.СохранитьАнкетуToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ИзменитьToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.УдалитьToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ДублироватьToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataSet1 = New System.Data.DataSet()
        Me.DataTable1 = New System.Data.DataTable()
        Me.DataColumn1 = New System.Data.DataColumn()
        Me.DataColumn2 = New System.Data.DataColumn()
        Me.DataColumn3 = New System.Data.DataColumn()
        Me.DataColumn4 = New System.Data.DataColumn()
        Me.DataColumn5 = New System.Data.DataColumn()
        Me.DataColumn6 = New System.Data.DataColumn()
        Me.DataColumn7 = New System.Data.DataColumn()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ListBox4 = New System.Windows.Forms.ListBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.scr_lab = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ActiveControl_Get = New System.Windows.Forms.Timer(Me.components)
        Me.cmd_service_wait = New System.Windows.Forms.Timer(Me.components)
        Me.Label20 = New System.Windows.Forms.Label()
        Me.StartFileCopy = New System.ComponentModel.BackgroundWorker()
        Me.Timer_timer = New System.Windows.Forms.Timer(Me.components)
        Me.DB1Analyser = New System.ComponentModel.BackgroundWorker()
        Me.Search_Elements = New System.ComponentModel.BackgroundWorker()
        Me.ExportToWord = New System.ComponentModel.BackgroundWorker()
        Me.DB1Background_save = New System.ComponentModel.BackgroundWorker()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenu_db1.SuspendLayout()
        CType(Me.DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(183, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Введите имя участника:"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(9, 22)
        Me.TextBox1.MaxLength = 1000
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(313, 20)
        Me.TextBox1.TabIndex = 2
        Me.TextBox1.Tag = "ToDisable"
        Me.Help.SetToolTip(Me.TextBox1, "Введите имя участника для распределения.")
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(9, 48)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(313, 23)
        Me.Button1.TabIndex = 6
        Me.Button1.Tag = "ToDisable"
        Me.Button1.Text = "Добавить к списку"
        Me.Help.SetToolTip(Me.Button1, "Добавить участника к списку участников.")
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.HorizontalScrollbar = True
        Me.ListBox1.Location = New System.Drawing.Point(3, 89)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(319, 147)
        Me.ListBox1.TabIndex = 7
        Me.ListBox1.Tag = "ToDisable"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(143, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Список учасников:"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(206, 242)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(116, 23)
        Me.Button2.TabIndex = 9
        Me.Button2.Tag = "ToDisable"
        Me.Button2.Text = "Удалить"
        Me.Help.SetToolTip(Me.Button2, "Удалить выбранный элемент списка.")
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(5, 286)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(119, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Распределение:"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(100, 242)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(100, 23)
        Me.Button3.TabIndex = 12
        Me.Button3.Tag = "ToDisable"
        Me.Button3.Text = "Изменить"
        Me.Help.SetToolTip(Me.Button3, "Изменить выбранный элемент списка.")
        Me.Button3.UseVisualStyleBackColor = True
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(349, 22)
        Me.TextBox3.MaxLength = 100
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(292, 20)
        Me.TextBox3.TabIndex = 16
        Me.TextBox3.Tag = "ToDisable"
        Me.Help.SetToolTip(Me.TextBox3, "Введите название команды.")
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(349, 48)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(292, 23)
        Me.Button4.TabIndex = 20
        Me.Button4.Tag = "ToDisable"
        Me.Button4.Text = "Добавить к списку"
        Me.Help.SetToolTip(Me.Button4, "Добавить команду в список команд.")
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(411, 243)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(99, 23)
        Me.Button5.TabIndex = 24
        Me.Button5.Tag = "ToDisable"
        Me.Button5.Text = "Изменить"
        Me.Help.SetToolTip(Me.Button5, "Изменить выбранный элемент списка.")
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(516, 243)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(125, 23)
        Me.Button6.TabIndex = 23
        Me.Button6.Tag = "ToDisable"
        Me.Button6.Text = "Удалить"
        Me.Help.SetToolTip(Me.Button6, "Удалить выбранный элемент списка.")
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(504, 302)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(137, 77)
        Me.Button7.TabIndex = 25
        Me.Button7.Tag = "ToDisable"
        Me.Button7.Text = "Распределить"
        Me.Help.SetToolTip(Me.Button7, "Выполнить распределение участников по командам." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Процесс может занять некоторое в" &
        "ремя.")
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Button8.Location = New System.Drawing.Point(504, 433)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(137, 29)
        Me.Button8.TabIndex = 26
        Me.Button8.Tag = "ToDisable"
        Me.Button8.Text = "Сброс"
        Me.Help.SetToolTip(Me.Button8, "Сбросить все введённые данные.")
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(504, 385)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(137, 42)
        Me.Button9.TabIndex = 27
        Me.Button9.Tag = "ToDisable"
        Me.Button9.Text = "Сохранить"
        Me.Help.SetToolTip(Me.Button9, "Сохранить результаты распределения в файл.")
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(331, 276)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(310, 23)
        Me.Button10.TabIndex = 28
        Me.Button10.Tag = "ToDisable"
        Me.Button10.Text = "Настройки распределения"
        Me.Help.SetToolTip(Me.Button10, "Удалить выбранный элемент списка.")
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.DarkGray
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkRed
        Me.Label2.Location = New System.Drawing.Point(130, 270)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(192, 29)
        Me.Label2.TabIndex = 29
        Me.Label2.Text = "!"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Help.SetToolTip(Me.Label2, "Текущее распределение не актуально!" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Необходимо провести новое распределение." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "--" &
        "-------------------------------------------------------------" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Shift + левый кли" &
        "к = сделать распределение неактивным")
        Me.Label2.Visible = False
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.DarkGray
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.DarkRed
        Me.Label10.Location = New System.Drawing.Point(130, 270)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(192, 29)
        Me.Label10.TabIndex = 58
        Me.Label10.Text = "!"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Help.SetToolTip(Me.Label10, "Текущее распределение не актуально!" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Необходимо провести новое распределение." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "--" &
        "-------------------------------------------------------------" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Shift + левый кли" &
        "к = сделать распределение неактивным")
        Me.Label10.Visible = False
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(331, 276)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(310, 23)
        Me.Button13.TabIndex = 57
        Me.Button13.Tag = "ToDisable"
        Me.Button13.Text = "Настройки распределения"
        Me.Help.SetToolTip(Me.Button13, "Удалить выбранный элемент списка.")
        Me.Button13.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(9, 22)
        Me.TextBox2.MaxLength = 1000
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(313, 20)
        Me.TextBox2.TabIndex = 38
        Me.TextBox2.Tag = "ToDisable"
        Me.Help.SetToolTip(Me.TextBox2, "Введите имя участника для распределения.")
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(504, 385)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(137, 42)
        Me.Button14.TabIndex = 56
        Me.Button14.Tag = "ToDisable"
        Me.Button14.Text = "Сохранить"
        Me.Help.SetToolTip(Me.Button14, "Сохранить результаты распределения в файл.")
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(9, 48)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(313, 23)
        Me.Button15.TabIndex = 39
        Me.Button15.Tag = "ToDisable"
        Me.Button15.Text = "Добавить к списку"
        Me.Help.SetToolTip(Me.Button15, "Добавить участника к списку участников.")
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.BackColor = System.Drawing.SystemColors.ControlDark
        Me.Button16.Location = New System.Drawing.Point(504, 433)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(137, 29)
        Me.Button16.TabIndex = 54
        Me.Button16.Tag = "ToDisable"
        Me.Button16.Text = "Сброс"
        Me.Help.SetToolTip(Me.Button16, "Сбросить все введённые данные.")
        Me.Button16.UseVisualStyleBackColor = False
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(504, 302)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(137, 77)
        Me.Button17.TabIndex = 53
        Me.Button17.Tag = "ToDisable"
        Me.Button17.Text = "Распределить"
        Me.Help.SetToolTip(Me.Button17, "Выполнить распределение участников по командам." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Процесс может занять некоторое в" &
        "ремя.")
        Me.Button17.UseVisualStyleBackColor = True
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(411, 243)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(99, 23)
        Me.Button18.TabIndex = 52
        Me.Button18.Tag = "ToDisable"
        Me.Button18.Text = "Изменить"
        Me.Help.SetToolTip(Me.Button18, "Изменить выбранный элемент списка.")
        Me.Button18.UseVisualStyleBackColor = True
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(206, 242)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(116, 23)
        Me.Button19.TabIndex = 42
        Me.Button19.Tag = "ToDisable"
        Me.Button19.Text = "Удалить"
        Me.Help.SetToolTip(Me.Button19, "Удалить выбранный элемент списка.")
        Me.Button19.UseVisualStyleBackColor = True
        '
        'Button20
        '
        Me.Button20.Location = New System.Drawing.Point(516, 243)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(125, 23)
        Me.Button20.TabIndex = 51
        Me.Button20.Tag = "ToDisable"
        Me.Button20.Text = "Удалить"
        Me.Help.SetToolTip(Me.Button20, "Удалить выбранный элемент списка.")
        Me.Button20.UseVisualStyleBackColor = True
        '
        'Button21
        '
        Me.Button21.Location = New System.Drawing.Point(100, 242)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(100, 23)
        Me.Button21.TabIndex = 44
        Me.Button21.Tag = "ToDisable"
        Me.Button21.Text = "Изменить"
        Me.Help.SetToolTip(Me.Button21, "Изменить выбранный элемент списка.")
        Me.Button21.UseVisualStyleBackColor = True
        '
        'Button22
        '
        Me.Button22.Location = New System.Drawing.Point(349, 48)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(292, 23)
        Me.Button22.TabIndex = 48
        Me.Button22.Tag = "ToDisable"
        Me.Button22.Text = "Добавить к списку"
        Me.Help.SetToolTip(Me.Button22, "Добавить команду в список команд.")
        Me.Button22.UseVisualStyleBackColor = True
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(349, 22)
        Me.TextBox4.MaxLength = 100
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(292, 20)
        Me.TextBox4.TabIndex = 47
        Me.TextBox4.Tag = "ToDisable"
        Me.Help.SetToolTip(Me.TextBox4, "Введите название команды.")
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(676, 477)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(121, 23)
        Me.Button11.TabIndex = 34
        Me.Button11.Text = "Убрать"
        Me.Help.SetToolTip(Me.Button11, "Удаляет выделенное сообщение." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "-----------------------------------------------" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "S" &
        "hift +Убрать = удалить все уведомления" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Shift + P + Убрать = удалить только неак" &
        "тивные уведомления")
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(676, 448)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(150, 23)
        Me.Button12.TabIndex = 35
        Me.Button12.Text = "Перейти"
        Me.Help.SetToolTip(Me.Button12, "Выполнить переход к объекту-отправителю уведомления.")
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button23
        '
        Me.Button23.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button23.Image = Global.MCD.My.Resources.Resources.set_mini
        Me.Button23.Location = New System.Drawing.Point(803, 478)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(23, 23)
        Me.Button23.TabIndex = 37
        Me.Help.SetToolTip(Me.Button23, "Настройки уведомлений.")
        Me.Button23.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.PictureBox1.Image = Global.MCD.My.Resources.Resources.pd
        Me.PictureBox1.Location = New System.Drawing.Point(753, 12)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(65, 48)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        Me.Help.SetToolTip(Me.PictureBox1, "О программе.")
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(257, 416)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(65, 17)
        Me.RadioButton2.TabIndex = 12
        Me.RadioButton2.Tag = ""
        Me.RadioButton2.Text = "Поиск"
        Me.Help.SetToolTip(Me.RadioButton2, resources.GetString("RadioButton2.ToolTip"))
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(257, 399)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(89, 17)
        Me.RadioButton1.TabIndex = 11
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Tag = ""
        Me.RadioButton1.Text = "Стандарт"
        Me.Help.SetToolTip(Me.RadioButton1, "Режим представления базы данных, в котором выводятся все её элементы." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Переключай" &
        "те вид с зажатой клавишей ""Shift"", чтобы выбранные в предыдущем виде элементы бы" &
        "ли выбраны и в новом.")
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Button31
        '
        Me.Button31.Location = New System.Drawing.Point(213, 434)
        Me.Button31.Name = "Button31"
        Me.Button31.Size = New System.Drawing.Size(133, 29)
        Me.Button31.TabIndex = 4
        Me.Button31.Tag = ""
        Me.Button31.Text = "Поиск"
        Me.Help.SetToolTip(Me.Button31, resources.GetString("Button31.ToolTip"))
        Me.Button31.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.MCD.My.Resources.Resources.saved
        Me.PictureBox2.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(56, 56)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox2.TabIndex = 6
        Me.PictureBox2.TabStop = False
        Me.Help.SetToolTip(Me.PictureBox2, "Изменения в базе данных сохранены.")
        '
        'Button33
        '
        Me.Button33.Location = New System.Drawing.Point(65, 3)
        Me.Button33.Name = "Button33"
        Me.Button33.Size = New System.Drawing.Size(132, 29)
        Me.Button33.TabIndex = 8
        Me.Button33.Tag = "ToDisableOnSave"
        Me.Button33.Text = "Сохранить"
        Me.Help.SetToolTip(Me.Button33, "Сохранить изменения в базе данных.")
        Me.Button33.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(65, 41)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(138, 17)
        Me.CheckBox1.TabIndex = 7
        Me.CheckBox1.Tag = "ToDisableOnSave"
        Me.CheckBox1.Text = "Автосохранение"
        Me.Help.SetToolTip(Me.CheckBox1, resources.GetString("CheckBox1.ToolTip"))
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'ListBox2
        '
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.HorizontalScrollbar = True
        Me.ListBox2.Location = New System.Drawing.Point(3, 302)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(495, 160)
        Me.ListBox2.TabIndex = 27
        Me.ListBox2.Tag = "ToDisable"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(328, 3)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(15, 273)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(349, 6)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(207, 13)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Введите название команды:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(349, 74)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(119, 13)
        Me.Label8.TabIndex = 21
        Me.Label8.Text = "Список команд:"
        '
        'ListBox3
        '
        Me.ListBox3.FormattingEnabled = True
        Me.ListBox3.HorizontalScrollbar = True
        Me.ListBox3.Location = New System.Drawing.Point(349, 90)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(292, 147)
        Me.ListBox3.TabIndex = 22
        Me.ListBox3.Tag = "ToDisable"
        '
        'Sorting_timer
        '
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(655, 493)
        Me.TabControl1.TabIndex = 30
        Me.TabControl1.Tag = "ToDisable"
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.Button10)
        Me.TabPage1.Controls.Add(Me.TextBox1)
        Me.TabPage1.Controls.Add(Me.Button9)
        Me.TabPage1.Controls.Add(Me.Button1)
        Me.TabPage1.Controls.Add(Me.Button8)
        Me.TabPage1.Controls.Add(Me.ListBox1)
        Me.TabPage1.Controls.Add(Me.Button7)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Button5)
        Me.TabPage1.Controls.Add(Me.Button2)
        Me.TabPage1.Controls.Add(Me.Button6)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.ListBox3)
        Me.TabPage1.Controls.Add(Me.Button3)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.ListBox2)
        Me.TabPage1.Controls.Add(Me.Button4)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.TextBox3)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Location = New System.Drawing.Point(4, 23)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(647, 466)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Команды    "
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Controls.Add(Me.Label16)
        Me.TabPage2.Controls.Add(Me.Button13)
        Me.TabPage2.Controls.Add(Me.TextBox4)
        Me.TabPage2.Controls.Add(Me.TextBox2)
        Me.TabPage2.Controls.Add(Me.Label15)
        Me.TabPage2.Controls.Add(Me.Button14)
        Me.TabPage2.Controls.Add(Me.Button22)
        Me.TabPage2.Controls.Add(Me.Button15)
        Me.TabPage2.Controls.Add(Me.ListBox7)
        Me.TabPage2.Controls.Add(Me.Button16)
        Me.TabPage2.Controls.Add(Me.Label14)
        Me.TabPage2.Controls.Add(Me.ListBox5)
        Me.TabPage2.Controls.Add(Me.Button21)
        Me.TabPage2.Controls.Add(Me.Button17)
        Me.TabPage2.Controls.Add(Me.ListBox6)
        Me.TabPage2.Controls.Add(Me.Label12)
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.Button18)
        Me.TabPage2.Controls.Add(Me.Button20)
        Me.TabPage2.Controls.Add(Me.Button19)
        Me.TabPage2.Location = New System.Drawing.Point(4, 23)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(647, 466)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Турниры  "
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 6)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(207, 13)
        Me.Label11.TabIndex = 37
        Me.Label11.Text = "Введите название команды:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(349, 6)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(207, 13)
        Me.Label16.TabIndex = 46
        Me.Label16.Text = "Введите название турнира:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(328, 3)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(15, 273)
        Me.Label15.TabIndex = 45
        Me.Label15.Text = "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "|"
        '
        'ListBox7
        '
        Me.ListBox7.FormattingEnabled = True
        Me.ListBox7.HorizontalScrollbar = True
        Me.ListBox7.Location = New System.Drawing.Point(3, 302)
        Me.ListBox7.Name = "ListBox7"
        Me.ListBox7.Size = New System.Drawing.Size(495, 160)
        Me.ListBox7.TabIndex = 55
        Me.ListBox7.Tag = "ToDisable"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(349, 74)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(135, 13)
        Me.Label14.TabIndex = 49
        Me.Label14.Text = "Список турниров:"
        '
        'ListBox5
        '
        Me.ListBox5.FormattingEnabled = True
        Me.ListBox5.HorizontalScrollbar = True
        Me.ListBox5.Location = New System.Drawing.Point(3, 89)
        Me.ListBox5.Name = "ListBox5"
        Me.ListBox5.Size = New System.Drawing.Size(319, 147)
        Me.ListBox5.TabIndex = 40
        Me.ListBox5.Tag = "ToDisable"
        '
        'ListBox6
        '
        Me.ListBox6.FormattingEnabled = True
        Me.ListBox6.HorizontalScrollbar = True
        Me.ListBox6.Location = New System.Drawing.Point(349, 90)
        Me.ListBox6.Name = "ListBox6"
        Me.ListBox6.Size = New System.Drawing.Size(292, 147)
        Me.ListBox6.TabIndex = 50
        Me.ListBox6.Tag = "ToDisable"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(3, 73)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(119, 13)
        Me.Label12.TabIndex = 41
        Me.Label12.Text = "Список команд:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(5, 286)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(119, 13)
        Me.Label13.TabIndex = 43
        Me.Label13.Text = "Распределение:"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.TabPage3.Controls.Add(Me.Button27)
        Me.TabPage3.Controls.Add(Me.Button26)
        Me.TabPage3.Controls.Add(Me.Button25)
        Me.TabPage3.Controls.Add(Me.Button24)
        Me.TabPage3.Controls.Add(Me.Label17)
        Me.TabPage3.Controls.Add(Me.Label22)
        Me.TabPage3.Controls.Add(Me.Label23)
        Me.TabPage3.Controls.Add(Me.Label19)
        Me.TabPage3.Controls.Add(Me.Label18)
        Me.TabPage3.Location = New System.Drawing.Point(4, 23)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(647, 466)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Таймер  "
        '
        'Button27
        '
        Me.Button27.Image = Global.MCD.My.Resources.Resources.set_mini
        Me.Button27.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button27.Location = New System.Drawing.Point(536, 433)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(108, 30)
        Me.Button27.TabIndex = 39
        Me.Button27.Text = "Настройки"
        Me.Button27.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button27.UseVisualStyleBackColor = True
        '
        'Button26
        '
        Me.Button26.Enabled = False
        Me.Button26.Image = Global.MCD.My.Resources.Resources.full_screen
        Me.Button26.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button26.Location = New System.Drawing.Point(492, 11)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(143, 40)
        Me.Button26.TabIndex = 38
        Me.Button26.Text = "Полный экран"
        Me.Button26.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button26.UseVisualStyleBackColor = True
        '
        'Button25
        '
        Me.Button25.Enabled = False
        Me.Button25.Location = New System.Drawing.Point(431, 433)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(99, 30)
        Me.Button25.TabIndex = 17
        Me.Button25.Text = "Сброс"
        Me.Button25.UseVisualStyleBackColor = True
        '
        'Button24
        '
        Me.Button24.Enabled = False
        Me.Button24.Font = New System.Drawing.Font("Lucida Console", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button24.Location = New System.Drawing.Point(3, 433)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(422, 30)
        Me.Button24.TabIndex = 16
        Me.Button24.Text = "Старт"
        Me.Button24.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Lucida Console", 72.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label17.Location = New System.Drawing.Point(16, 155)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(619, 89)
        Me.Label17.TabIndex = 15
        Me.Label17.Text = "00:00:00"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(234, 20)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(127, 13)
        Me.Label22.TabIndex = 14
        Me.Label22.Text = "Прошло времени:"
        '
        'Label23
        '
        Me.Label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label23.Location = New System.Drawing.Point(367, 16)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(78, 21)
        Me.Label23.TabIndex = 13
        Me.Label23.Text = "00:00:00"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(13, 20)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(127, 13)
        Me.Label19.TabIndex = 10
        Me.Label19.Text = "Исходное время:"
        '
        'Label18
        '
        Me.Label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label18.Location = New System.Drawing.Point(146, 16)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(78, 21)
        Me.Label18.TabIndex = 9
        Me.Label18.Text = "00:00:00"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.TabPage4.Controls.Add(Me.PictureBox3)
        Me.TabPage4.Controls.Add(Me.Label24)
        Me.TabPage4.Controls.Add(Me.Label21)
        Me.TabPage4.Controls.Add(Me.RadioButton2)
        Me.TabPage4.Controls.Add(Me.RadioButton1)
        Me.TabPage4.Controls.Add(Me.Button31)
        Me.TabPage4.Controls.Add(Me.Panel3)
        Me.TabPage4.Controls.Add(Me.Button30)
        Me.TabPage4.Controls.Add(Me.Button29)
        Me.TabPage4.Controls.Add(Me.Button28)
        Me.TabPage4.Controls.Add(Me.DataGridView1)
        Me.TabPage4.Location = New System.Drawing.Point(4, 23)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(647, 466)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "База данных"
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.MCD.My.Resources.Resources.db_load
        Me.PictureBox3.Location = New System.Drawing.Point(297, 176)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(64, 64)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox3.TabIndex = 14
        Me.PictureBox3.TabStop = False
        '
        'Label24
        '
        Me.Label24.Location = New System.Drawing.Point(190, 197)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(101, 25)
        Me.Label24.TabIndex = 15
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(212, 400)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(39, 13)
        Me.Label21.TabIndex = 13
        Me.Label21.Text = "Вид:"
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.PictureBox2)
        Me.Panel3.Controls.Add(Me.CheckBox1)
        Me.Panel3.Controls.Add(Me.Button33)
        Me.Panel3.Location = New System.Drawing.Point(3, 399)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(202, 64)
        Me.Panel3.TabIndex = 10
        '
        'Button30
        '
        Me.Button30.Location = New System.Drawing.Point(515, 434)
        Me.Button30.Name = "Button30"
        Me.Button30.Size = New System.Drawing.Size(129, 29)
        Me.Button30.TabIndex = 3
        Me.Button30.Tag = "ToDisableOnSave"
        Me.Button30.Text = "Удалить"
        Me.Button30.UseVisualStyleBackColor = True
        '
        'Button29
        '
        Me.Button29.Location = New System.Drawing.Point(371, 399)
        Me.Button29.Name = "Button29"
        Me.Button29.Size = New System.Drawing.Size(273, 29)
        Me.Button29.TabIndex = 2
        Me.Button29.Tag = "ToDisableOnSave"
        Me.Button29.Text = "Просмотр"
        Me.Button29.UseVisualStyleBackColor = True
        '
        'Button28
        '
        Me.Button28.Location = New System.Drawing.Point(371, 434)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(138, 29)
        Me.Button28.TabIndex = 1
        Me.Button28.Tag = "ToDisableOnSave"
        Me.Button28.Text = "Добавить"
        Me.Button28.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToResizeRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Key, Me.ImageDataGridViewTextBoxColumn, Me.NameDataGridViewTextBoxColumn, Me.SchoolDataGridViewTextBoxColumn, Me.YearsDataGridViewTextBoxColumn, Me.TelephoneDataGridViewTextBoxColumn, Me.PostDataGridViewTextBoxColumn})
        Me.DataGridView1.ContextMenuStrip = Me.ContextMenu_db1
        Me.DataGridView1.DataMember = "DataTable1"
        Me.DataGridView1.DataSource = Me.DataSet1
        Me.DataGridView1.EnableHeadersVisualStyles = False
        Me.DataGridView1.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.RowTemplate.Height = 32
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.ShowCellErrors = False
        Me.DataGridView1.ShowCellToolTips = False
        Me.DataGridView1.ShowEditingIcon = False
        Me.DataGridView1.ShowRowErrors = False
        Me.DataGridView1.Size = New System.Drawing.Size(641, 390)
        Me.DataGridView1.TabIndex = 0
        Me.DataGridView1.Tag = ""
        '
        'Key
        '
        Me.Key.DataPropertyName = "Key"
        Me.Key.Frozen = True
        Me.Key.HeaderText = "Key"
        Me.Key.MinimumWidth = 10
        Me.Key.Name = "Key"
        Me.Key.ReadOnly = True
        Me.Key.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Key.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Key.Visible = False
        Me.Key.Width = 10
        '
        'ImageDataGridViewTextBoxColumn
        '
        Me.ImageDataGridViewTextBoxColumn.DataPropertyName = "Image"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.NullValue = CType(resources.GetObject("DataGridViewCellStyle1.NullValue"), Object)
        Me.ImageDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle1
        Me.ImageDataGridViewTextBoxColumn.DividerWidth = 2
        Me.ImageDataGridViewTextBoxColumn.Frozen = True
        Me.ImageDataGridViewTextBoxColumn.HeaderText = "Фото"
        Me.ImageDataGridViewTextBoxColumn.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom
        Me.ImageDataGridViewTextBoxColumn.MinimumWidth = 45
        Me.ImageDataGridViewTextBoxColumn.Name = "ImageDataGridViewTextBoxColumn"
        Me.ImageDataGridViewTextBoxColumn.ReadOnly = True
        Me.ImageDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ImageDataGridViewTextBoxColumn.Width = 45
        '
        'NameDataGridViewTextBoxColumn
        '
        Me.NameDataGridViewTextBoxColumn.DataPropertyName = "Name"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        Me.NameDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.NameDataGridViewTextBoxColumn.HeaderText = "Имя"
        Me.NameDataGridViewTextBoxColumn.MinimumWidth = 64
        Me.NameDataGridViewTextBoxColumn.Name = "NameDataGridViewTextBoxColumn"
        Me.NameDataGridViewTextBoxColumn.ReadOnly = True
        Me.NameDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.NameDataGridViewTextBoxColumn.Width = 64
        '
        'SchoolDataGridViewTextBoxColumn
        '
        Me.SchoolDataGridViewTextBoxColumn.DataPropertyName = "School"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro
        Me.SchoolDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.SchoolDataGridViewTextBoxColumn.HeaderText = "Школа"
        Me.SchoolDataGridViewTextBoxColumn.Name = "SchoolDataGridViewTextBoxColumn"
        Me.SchoolDataGridViewTextBoxColumn.ReadOnly = True
        Me.SchoolDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.SchoolDataGridViewTextBoxColumn.Width = 72
        '
        'YearsDataGridViewTextBoxColumn
        '
        Me.YearsDataGridViewTextBoxColumn.DataPropertyName = "Years"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Gainsboro
        Me.YearsDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.YearsDataGridViewTextBoxColumn.HeaderText = "Дата рождения"
        Me.YearsDataGridViewTextBoxColumn.Name = "YearsDataGridViewTextBoxColumn"
        Me.YearsDataGridViewTextBoxColumn.ReadOnly = True
        Me.YearsDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.YearsDataGridViewTextBoxColumn.Width = 140
        '
        'TelephoneDataGridViewTextBoxColumn
        '
        Me.TelephoneDataGridViewTextBoxColumn.DataPropertyName = "Telephone"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Gainsboro
        Me.TelephoneDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.TelephoneDataGridViewTextBoxColumn.HeaderText = "Телефон"
        Me.TelephoneDataGridViewTextBoxColumn.Name = "TelephoneDataGridViewTextBoxColumn"
        Me.TelephoneDataGridViewTextBoxColumn.ReadOnly = True
        Me.TelephoneDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.TelephoneDataGridViewTextBoxColumn.Width = 88
        '
        'PostDataGridViewTextBoxColumn
        '
        Me.PostDataGridViewTextBoxColumn.DataPropertyName = "Post"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.Gainsboro
        Me.PostDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle6
        Me.PostDataGridViewTextBoxColumn.HeaderText = "Должность"
        Me.PostDataGridViewTextBoxColumn.Name = "PostDataGridViewTextBoxColumn"
        Me.PostDataGridViewTextBoxColumn.ReadOnly = True
        Me.PostDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.PostDataGridViewTextBoxColumn.Width = 104
        '
        'ContextMenu_db1
        '
        Me.ContextMenu_db1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.ContextMenu_db1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ПросмотрToolStripMenuItem, Me.ToolStripSeparator1, Me.ДобавитьКРаспределениюToolStripMenuItem, Me.СохранитьАнкетуToolStripMenuItem, Me.ToolStripSeparator2, Me.ИзменитьToolStripMenuItem1, Me.УдалитьToolStripMenuItem1, Me.ДублироватьToolStripMenuItem})
        Me.ContextMenu_db1.Name = "ContextMenu_db1"
        Me.ContextMenu_db1.ShowImageMargin = False
        Me.ContextMenu_db1.Size = New System.Drawing.Size(220, 148)
        Me.ContextMenu_db1.Tag = "ToDisableOnSave"
        '
        'ПросмотрToolStripMenuItem
        '
        Me.ПросмотрToolStripMenuItem.Name = "ПросмотрToolStripMenuItem"
        Me.ПросмотрToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.ПросмотрToolStripMenuItem.Text = "Просмотр"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(216, 6)
        '
        'ДобавитьКРаспределениюToolStripMenuItem
        '
        Me.ДобавитьКРаспределениюToolStripMenuItem.Name = "ДобавитьКРаспределениюToolStripMenuItem"
        Me.ДобавитьКРаспределениюToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.ДобавитьКРаспределениюToolStripMenuItem.Text = "Добавить к распределению"
        '
        'СохранитьАнкетуToolStripMenuItem
        '
        Me.СохранитьАнкетуToolStripMenuItem.Name = "СохранитьАнкетуToolStripMenuItem"
        Me.СохранитьАнкетуToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.СохранитьАнкетуToolStripMenuItem.Text = "Сохранить данные в Word"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(216, 6)
        '
        'ИзменитьToolStripMenuItem1
        '
        Me.ИзменитьToolStripMenuItem1.Name = "ИзменитьToolStripMenuItem1"
        Me.ИзменитьToolStripMenuItem1.Size = New System.Drawing.Size(219, 22)
        Me.ИзменитьToolStripMenuItem1.Tag = "ToDisableOnSave"
        Me.ИзменитьToolStripMenuItem1.Text = "Изменить"
        '
        'УдалитьToolStripMenuItem1
        '
        Me.УдалитьToolStripMenuItem1.Name = "УдалитьToolStripMenuItem1"
        Me.УдалитьToolStripMenuItem1.Size = New System.Drawing.Size(219, 22)
        Me.УдалитьToolStripMenuItem1.Tag = "ToDisableOnSave"
        Me.УдалитьToolStripMenuItem1.Text = "Удалить"
        '
        'ДублироватьToolStripMenuItem
        '
        Me.ДублироватьToolStripMenuItem.Name = "ДублироватьToolStripMenuItem"
        Me.ДублироватьToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.ДублироватьToolStripMenuItem.Text = "Дублировать"
        '
        'DataSet1
        '
        Me.DataSet1.DataSetName = "DataTable"
        Me.DataSet1.Tables.AddRange(New System.Data.DataTable() {Me.DataTable1})
        '
        'DataTable1
        '
        Me.DataTable1.Columns.AddRange(New System.Data.DataColumn() {Me.DataColumn1, Me.DataColumn2, Me.DataColumn3, Me.DataColumn4, Me.DataColumn5, Me.DataColumn6, Me.DataColumn7})
        Me.DataTable1.TableName = "DataTable1"
        '
        'DataColumn1
        '
        Me.DataColumn1.ColumnName = "Image"
        Me.DataColumn1.DataType = GetType(Object)
        '
        'DataColumn2
        '
        Me.DataColumn2.ColumnName = "Name"
        '
        'DataColumn3
        '
        Me.DataColumn3.ColumnName = "Years"
        '
        'DataColumn4
        '
        Me.DataColumn4.ColumnName = "School"
        '
        'DataColumn5
        '
        Me.DataColumn5.ColumnName = "Telephone"
        '
        'DataColumn6
        '
        Me.DataColumn6.ColumnName = "Post"
        '
        'DataColumn7
        '
        Me.DataColumn7.ColumnName = "Key"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(669, 73)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(135, 13)
        Me.Label7.TabIndex = 31
        Me.Label7.Text = "Уведомления (0):"
        '
        'ListBox4
        '
        Me.ListBox4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ListBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ListBox4.FormattingEnabled = True
        Me.ListBox4.Location = New System.Drawing.Point(3, 3)
        Me.ListBox4.Name = "ListBox4"
        Me.ListBox4.Size = New System.Drawing.Size(120, 312)
        Me.ListBox4.TabIndex = 32
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.ListBox4)
        Me.Panel1.Controls.Add(Me.scr_lab)
        Me.Panel1.Location = New System.Drawing.Point(673, 89)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(153, 353)
        Me.Panel1.TabIndex = 33
        '
        'scr_lab
        '
        Me.scr_lab.AutoSize = True
        Me.scr_lab.Location = New System.Drawing.Point(3, 302)
        Me.scr_lab.Name = "scr_lab"
        Me.scr_lab.Size = New System.Drawing.Size(55, 13)
        Me.scr_lab.TabIndex = 37
        Me.scr_lab.Text = "Scroll"
        Me.scr_lab.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(673, 47)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(15, 13)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "0"
        Me.Label9.Visible = False
        '
        'ActiveControl_Get
        '
        Me.ActiveControl_Get.Enabled = True
        '
        'cmd_service_wait
        '
        Me.cmd_service_wait.Interval = 500
        '
        'Label20
        '
        Me.Label20.Font = New System.Drawing.Font("Lucida Console", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label20.Location = New System.Drawing.Point(673, 12)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(56, 48)
        Me.Label20.TabIndex = 38
        Me.Label20.Text = "Идёт загрузка..."
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'StartFileCopy
        '
        '
        'Timer_timer
        '
        Me.Timer_timer.Interval = 1000
        '
        'DB1Analyser
        '
        '
        'Search_Elements
        '
        '
        'ExportToWord
        '
        '
        'DB1Background_save
        '
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(838, 514)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Button23)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label9)
        Me.Font = New System.Drawing.Font("Lucida Console", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MaximizeBox = False
        Me.Name = "Main"
        Me.Text = "Распределитель участников клуба"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenu_db1.ResumeLayout(False)
        CType(Me.DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataTable1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents ListBox1 As ListBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents Help As ToolTip
    Friend WithEvents ListBox2 As ListBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents Button4 As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents ListBox3 As ListBox
    Friend WithEvents Button5 As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents Button7 As Button
    Friend WithEvents Button8 As Button
    Friend WithEvents Button9 As Button
    Friend WithEvents Sorting_timer As Timer
    Friend WithEvents Button10 As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents Label7 As Label
    Friend WithEvents ListBox4 As ListBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Button11 As Button
    Friend WithEvents Button12 As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Button13 As Button
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Button14 As Button
    Friend WithEvents Button22 As Button
    Friend WithEvents Button15 As Button
    Friend WithEvents ListBox7 As ListBox
    Friend WithEvents Button16 As Button
    Friend WithEvents Label14 As Label
    Friend WithEvents ListBox5 As ListBox
    Friend WithEvents Button21 As Button
    Friend WithEvents Button17 As Button
    Friend WithEvents ListBox6 As ListBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Button18 As Button
    Friend WithEvents Button20 As Button
    Friend WithEvents Button19 As Button
    Friend WithEvents scr_lab As Label
    Friend WithEvents ActiveControl_Get As Timer
    Friend WithEvents Button23 As Button
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents Button24 As Button
    Friend WithEvents Button25 As Button
    Friend WithEvents Button26 As Button
    Friend WithEvents Button27 As Button
    Friend WithEvents cmd_service_wait As Timer
    Friend WithEvents Label20 As Label
    Friend WithEvents StartFileCopy As System.ComponentModel.BackgroundWorker
    Friend WithEvents Timer_timer As Timer
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents DataSet1 As DataSet
    Friend WithEvents DataTable1 As DataTable
    Friend WithEvents DataColumn1 As DataColumn
    Friend WithEvents DataColumn2 As DataColumn
    Friend WithEvents DataColumn3 As DataColumn
    Friend WithEvents DataColumn4 As DataColumn
    Friend WithEvents DataColumn5 As DataColumn
    Friend WithEvents DataColumn6 As DataColumn
    Friend WithEvents Button30 As Button
    Friend WithEvents Button29 As Button
    Friend WithEvents Button28 As Button
    Friend WithEvents Button31 As Button
    Friend WithEvents DataColumn7 As DataColumn
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents Button33 As Button
    Friend WithEvents CheckBox1 As CheckBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents Label21 As Label
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents Label24 As Label
    Friend WithEvents DB1Analyser As System.ComponentModel.BackgroundWorker
    Friend WithEvents ContextMenu_db1 As ContextMenuStrip
    Friend WithEvents ИзменитьToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ПросмотрToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ДобавитьКРаспределениюToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents УдалитьToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents Search_Elements As System.ComponentModel.BackgroundWorker
    Friend WithEvents СохранитьАнкетуToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ДублироватьToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExportToWord As System.ComponentModel.BackgroundWorker
    Friend WithEvents Key As DataGridViewTextBoxColumn
    Friend WithEvents ImageDataGridViewTextBoxColumn As DataGridViewImageColumn
    Friend WithEvents NameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SchoolDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents YearsDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TelephoneDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PostDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DB1Background_save As System.ComponentModel.BackgroundWorker
End Class
